<?php

    class headway_social_iconBlockOptions extends HeadwayBlockOptionsAPI {

          public $tabs = array(
           // 'my-only-tab' => 'Settings', 
            'custom-icons' => 'Social Icons'
			
        );

        public $inputs = array(

			'custom-icons' => array(
				'icons' => array(
					'type' => 'repeater',
					'name' => 'icons',
					'label' => 'Icons',
					'inputs' => array(
						
						 array(
							'type' => 'select',
							'name' => 'image',
							'label' => 'Icon',
							'default' => 0,
							// 'tooltip' => '',
							'options' => array(
								'fa fa-facebook' => 'Facebook',
								'fa fa-facebook-square' => 'Facebook square',
								'fa fa-twitter' => 'Twitter',
								'fa fa-pinterest' => 'Pinterest',
								'fa fa-pinterest-square' => 'Pinterest square',
								'fa fa-android' => 'Android',
								'fa fa-tumblr' => 'Tumblr',
								'fa fa-tumblr-square' => 'Tumblr square',
								'fa fa-foursquare' => 'Foursquare',
								'fa fa-linkedin' => 'Linked in',
								'fa fa-github' => 'Github',
								'fa fa-github-alt' => 'Github alt',
								'fa fa-weibo' => 'Weibo',
								'fa fa-xing' => 'Xing',
								'fa fa-xing' => 'Xing square',
								'fa fa-stack-exchange' => 'Stack exchange',
								'fa fa-github-square' => 'Github square',
								'fa fa-html5' => 'Html 5',
								'fa fa-gittip' => 'Gittip',
								'fa fa-bitbucket' => 'Bitbucket',
								'fa fa-css3' => 'CSS 3',
								'fa fa-dribbble' => 'Dribbble',
								'fa fa-trello' => 'Trello',
								'fa fa-renren' => 'Renren',
								'fa fa-linux' => 'Linux',
								'fa fa-stack-overflow' => 'Overflow',
								'fa fa-windows' => 'Windows',
								'fa fa-maxcdn' => 'Max CDN',
								'fa fa-linkedin-square' => 'Linked in square',
								'fa fa-youtube-square' => 'Youtube square',
								'fa fa-youtube-play' => 'Youtube play',
								'fa fa-youtube' => 'Youtube',
								'fa fa-dropbox' => 'Dropbox',
								'fa fa-apple' => 'Apple',
								'fa fa-google-plus' => 'Google+',
								'fa fa-google-plus-square' => 'Google+ square',
								'fa fa-instagram' => 'Instagram',
								'fa fa-skype' => 'Skype',
								'fa fa-vimeo-square' => 'Vimeo square',
							)
						),

						array(
							'type' => 'text',
							'name' => 'image-title',
							'label' => '"title"',
							// 'tooltip' => ''
						),

						array(
							'type' => 'textarea',
							'name' => 'image-alt',
							'label' => '"alt"',
							'tooltip' => 'This will be used as the "alt" attribute for the image.  The alt attribute is <em>hugely</em> beneficial for SEO (Search Engine Optimization) and for general accessibility.'
						),

						array(
							'name' => 'link-heading',
							'type' => 'heading',
							'label' => 'Link Image'
						),

						array(
							'name' => 'link-url',
							'label' => 'Link URL?',
							'type' => 'text',
							'tooltip' => 'Set the URL for the image to link to'
						),

						array(
							'name' => 'link-alt',
							'label' => '"alt"',
							'type' => 'text',
							'tooltip' => 'Set alternative text for the link'
						),

						array(
							'name' => 'link-target',
							'label' => 'New window?',
							'type' => 'checkbox',
							'tooltip' => 'If you would like to open the link in a new window check this option',
							'default' => false,
						)

					),
					//'tooltip' => 'Upload the images that you would like to add to the image block.',
					'sortable' => true,
					'limit' => false
				),

				'display-inline'	=> array(
					'name'	 => 'display-inline',
					'type'	 => 'checkbox',
					'label'	 => 'Display Inline',
					'default' => false,
					// 'tooltip' => 'tooltip'
				),
		

				'heading-debug'	=> array(
					'name'	 => 'heading-debug',
					'type'	 => 'heading',
					'label'	 => 'Debug'
				),


				'debug'	=> array(
					'name'	 => 'debug',
					'type'	 => 'checkbox',
					'label'	 => 'Show debug info',
					'default' => false,
					// 'tooltip' => 'tooltip'
				),
				

			),
				
        );
    }