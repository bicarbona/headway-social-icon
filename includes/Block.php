<?php

class headway_social_iconBlock extends HeadwayBlockAPI {

    public $id = 'headway_social_icon';
    public $name = 'Social Icon';
    public $options_class = 'headway_social_iconBlockOptions';
    public $description = 'Social font icon';

    
	function enqueue_action($block_id) {

		/* CSS */
	//	wp_enqueue_style('headway-pin-board', plugin_dir_url(__FILE__) . '/css/pin-board.css');		

		/* JS */
	//	wp_enqueue_script('headway-pin-board', plugin_dir_url(__FILE__) . '/js/pin-board.js', array('jquery'));		

	}
	
	// public static function init_action($block_id, $block) 
    // {

    // }


    // public static function enqueue_action($block_id, $block, $original_block = null)
    // {

    // }


	// Pre zobrazenie podsebou / vedla seba

	public static function dynamic_css($block_id, $block, $original_block = null) {
	
	$display_inline = parent::get_setting($block, 'display-inline' , false);
	
		if ($display_inline == TRUE){
					return '#block-' . $block_id . ' ul li {display: inline-block;}';
		} else{
					return '#block-' . $block_id . ' ul li { display: list-item; }';
		}
	}


	// function dynamic_js($block_id, $block = false) {
	// 
	// 	if ( !$block )
	// 		$block = HeadwayBlocksData::get_block($block_id);
	// 
	// 	$js = "
	// 	jQuery(document).ready(function() {
	// 		
	// 	});
	// 	";
	// 
	// 	return $js;
	// 
	// }

    public function setup_elements() {
        $this->register_block_element(array(
            'id' => 'a-icon-fa',
            'name' => 'Link Icon',
            'selector' => 'a .fa',
            'states' => array(
                'Hover' => 'a:hover .fa',
                )
            ));
 

			$this->register_block_element(array(
				'id' => 'a-icon-fa-facebook',
				'name' => 'Link Icon Facebook',
				'selector' => 'a .fa-facebook',
				'states' => array(
				    'Hover' => 'a:hover .fa-facebook',
				    )
			));  

        $this->register_block_element(array(
            'id' => 'icon-fa',
            'name' => 'Icon Fa',
            'selector' => '.fa',
            'states' => array(
                'Hover' => '.fa:hover',
                )
            ));
    }

    public function content($block) {
        /* CODE HERE */
		$icons = parent::get_setting($block, 'icons' , array());
		$debug = parent::get_setting($block, 'debug' , false);

		foreach ( $icons as $icon ) {

			if ( headway_get('image', $icon) || headway_get('network', $icon) ) {
				$has_icons = true;
				break;
			}

		}

		if ( !$has_icons) {

			echo '<div class="alert alert-yellow"><p>There are no icons to display.</p></div>';
			
			return;

		}

		echo '<ul class="social-icons clearfix">';

			$i = 0;
		  	foreach ( $icons as $icon ) {

		  		if ( !headway_get('image', $icon) && !headway_get('network', $icon) )
		  			continue;

		  		//if ($icon_set == 'custom') {
					
				$img_url =	headway_fix_data_type(headway_get('image', $icon));
		  		//	$img_url = $icon['image'];
		  		// } else {
		  		// 	$img_url = headway_url().'/library/blocks/social/icons/' . $icon_set . '/' . headway_fix_data_type(headway_get('network', $icon));
		  		// }

		  		$i++;
		  		$output = array(
		  			'image' => array(
		  				'src' => $img_url,
		  				'alt' => headway_fix_data_type(headway_get('image-alt', $icon, false)) ? ' alt="' . headway_fix_data_type(headway_get('image-alt', $icon, false)) . '"' : null,
		  				'title' => headway_fix_data_type(headway_get('image-title', $icon)) ? ' title="' . headway_fix_data_type(headway_get('image-title', $icon)) . '"' : null,
		  			),

		  			'hyperlink' => array(
		  				'href' => headway_fix_data_type(headway_get('link-url', $icon)),
		  				'alt' => headway_fix_data_type(headway_get('link-alt', $icon, false)) ? ' alt="' . headway_fix_data_type(headway_get('link-alt', $icon, false)) . '"' : null,
		  				'target' => headway_fix_data_type(headway_get('link-target', $icon, false)) ? ' target="_blank"' : null
		  			)
		  		);

		  			echo '<li>';
				

					
		  			/* Open hyperlink if user added one for image */
		  			if ( $output['hyperlink']['href'] )
		  				echo '<a href="' . $output['hyperlink']['href'] . '"' . $output['hyperlink']['target'] . $output['hyperlink']['alt'] . '>';

				  			/* Don't forget to display the ACTUAL IMAGE */
				  			echo '<i ' . $output['image']['alt'] . $output['image']['title'] . ' class="'. $output['image']['src'] .' ' . $i . '" ' . $svg_width . ' ></i>';
							
				  			/* Don't forget to display the ACTUAL IMAGE */
				  	//		echo '<i src="' . $output['image']['src'] . '"' . $output['image']['alt'] . $output['image']['title'] . ' class="img-' . $i . '" ' . $svg_width . ' />';

		  			/* Closing tag for hyperlink */
		  			if ( $output['hyperlink']['href'] )
		  				echo '</a>';
				if ($debug){
					echo $icon['image'];
				}
		  			echo '</li>';
		  		
		  	}
	  
	  	echo '</ul>';
		
		
    }
}