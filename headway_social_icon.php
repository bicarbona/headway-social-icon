<?php
/**
 * Plugin Name: Headway Social Icon
 * Plugin URI:  https://bitbucket.org/bicarbona/social-icon
 * Description: Social font icon
 * Version:     0.1.3
 * Author:      Bicarbona
 * Author URI:  
 * License:     GPLv2+
 * Text Domain: headway_social_icon
 * Domain Path: /languages
 * Bitbucket Plugin URI: https://bitbucket.org/bicarbona/social-icon
 * Bitbucket Branch: master
 */



/**
 * Everything is ran at the after_setup_theme action to insure that all of Headway's classes and functions are loaded.
 **/
add_action('after_setup_theme', 'headway_social_icon_register');
function headway_social_icon_register() {

	if ( !class_exists('Headway') )
		return;

	require_once 'includes/Block.php';
	require_once 'includes/BlockOptions.php';
	//require_once 'design-editor-settings.php';

	return headway_register_block('headway_social_iconBlock', plugins_url(false, __FILE__));
}

/**
 * Prevent 404ing from breaking Infinite Scrolling
 **/
add_action('status_header', 'headway_social_icon_prevent_404');
function headway_social_icon_prevent_404($status) {

	if ( strpos($status, '404') && get_query_var('paged') && headway_get('pb') )
		return 'HTTP/1.1 200 OK';

	return $status;
}

/**
 * Prevent WordPress redirect from messing up featured board pagination
 */
add_filter('redirect_canonical', 'headway_social_icon_redirect');
function headway_social_icon_redirect($redirect_url) {

	if ( headway_get('pb') )
		return false;

	return $redirect_url;
}